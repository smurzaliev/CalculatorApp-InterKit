//
//  RealmManager.swift
//  CalculatorApp-InterKit
//
//  Created by Samat Murzaliev on 25.05.2022.
//

import Foundation
import RealmSwift

public class RealmManager {
    private let database: Realm
    static let sharedInstance = RealmManager()
    
    private init() {
        do {
            database = try Realm()
        }
        catch {
            fatalError("Error initializing Realm \(error.localizedDescription)")
        }
    }
    
    public func fetch<HistoryModel: Object>(object: HistoryModel) -> Results<HistoryModel> {
        return database.objects(HistoryModel.self)
    }
    
    public func save<HistoryModel: Object>(object: HistoryModel, _ errorHandler: @escaping ((_ error : Swift.Error) -> Void) = { _ in return }) {
        do {
            try database.write {
                database.add(object)
            }
        }
        catch {
            fatalError("Error saving Realm \(error.localizedDescription)")
        }
    }
    
    public func deleteAll(errorHandler: @escaping ((_ error : Swift.Error) -> Void) = { _ in return }) {
        do {
            try database.write {
                database.deleteAll()
            }
        }
        catch {
            fatalError("Error deleting Realm \(error.localizedDescription)")
        }
    }
}
