//
//  HistoryViewController.swift
//  CalculatorApp-InterKit
//
//  Created by Samat Murzaliev on 25.05.2022.
//

import UIKit
import SnapKit
import RealmSwift

class HistoryViewController: UIViewController, HistoryViewProtocol {
    
    var presenter: HistoryViewPresenterProtocol!
    var historyArray = RealmManager.sharedInstance.fetch(object: HistoryModel())
    
    private lazy var historyLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 30, weight: .regular)
        view.textColor = UIColor(named: "itemTintColor")
        view.text = "History"
        return view
    }()
    
    private lazy var clearButton: UIButton = {
        let view = UIButton(type: .system)
        let configutation = UIButton.Configuration.filled()
        view.configuration = configutation
        view.setTitle("Clear", for: .normal)
        view.titleLabel?.font = .systemFont(ofSize: 30, weight: .regular)
        view.tintColor = .systemRed
        view.addTarget(self, action: #selector(clearPressed(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var historyTable: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor(named: "buttonBackColor")
        view.separatorStyle = .singleLine
        view.dataSource = self
        view.delegate = self
        view.allowsSelection = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setSubviews()
        presenter = HistoryPresenter(view: self)
//        historyTable.scrollToBottomRow()
    }
    
    private func setSubviews() {
        view.backgroundColor = UIColor(named: "backgoundColor")
        
        view.addSubview(historyLabel)
        historyLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(13)
            make.left.equalToSuperview().offset(10)
        }
        
        view.addSubview(clearButton)
        clearButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-20)
        }
        
        view.addSubview(historyTable)
        historyTable.snp.makeConstraints { make in
            make.top.equalTo(clearButton.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(5)
            make.right.equalToSuperview().offset(-5)
            make.bottom.equalToSuperview()
        }
    }
    
    @objc func clearPressed(view: UIButton) {
        presenter.clearHistory()
        historyTable.reloadData()
    }
}

extension HistoryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HistoryTableCell()
        let item = historyArray[indexPath.row]
        cell.fill(item.calculationLine, item.resultLine)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
