//
//  HistoryPresenter.swift
//  CalculatorApp-InterKit
//
//  Created by Samat Murzaliev on 25.05.2022.
//

import Foundation
import RealmSwift

protocol HistoryViewProtocol: AnyObject {
}

protocol HistoryViewPresenterProtocol: AnyObject {
    init(view: HistoryViewProtocol)
    func clearHistory()
    func loadHistory() -> Results<HistoryModel>
}

class HistoryPresenter: HistoryViewPresenterProtocol {
    func loadHistory() -> Results<HistoryModel> {
        return dataBaseManager.fetch(object: HistoryModel())
    }
    
    let view: HistoryViewProtocol
    let dataBaseManager = RealmManager.sharedInstance

    required init(view: HistoryViewProtocol) {
        self.view = view
    }
    
    func clearHistory() {
        dataBaseManager.deleteAll()
    } 
}
