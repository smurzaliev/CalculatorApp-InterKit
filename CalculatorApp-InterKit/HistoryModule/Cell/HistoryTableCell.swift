//
//  HistoryTableCell.swift
//  CalculatorApp-InterKit
//
//  Created by Samat Murzaliev on 25.05.2022.
//

import UIKit

class HistoryTableCell: UITableViewCell {
    
    private lazy var calculationLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 22, weight: .thin)
        view.textColor = UIColor(named: "itemTintColor")
        view.textAlignment = .right
        view.adjustsFontSizeToFitWidth = true
        view.sizeToFit()
        return view
    }()
    
    private lazy var resultLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 22, weight: .regular)
        view.textColor = UIColor(named: "itemTintColor")
        view.textAlignment = .right
        view.adjustsFontSizeToFitWidth = true
        return view
    }()
    
    override func layoutSubviews() {
        setSubviews()
    }
    
    private func setSubviews() {
        addSubview(calculationLabel)
        calculationLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(3)
            make.right.equalToSuperview().offset(-6)
        }
        
        addSubview(resultLabel)
        resultLabel.snp.makeConstraints { make in
            make.top.equalTo(calculationLabel.snp.bottom).offset(3)
            make.right.equalToSuperview().offset(-6)
        }
    }
    
    func fill(_ calculationLine: String, _ resultLine: String) {
        calculationLabel.text = calculationLine
        resultLabel.text = "=\(resultLine)"
    }
}
