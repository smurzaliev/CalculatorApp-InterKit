//
//  HistoryModel.swift
//  CalculatorApp-InterKit
//
//  Created by Samat Murzaliev on 25.05.2022.
//

import Foundation
import RealmSwift

class HistoryModel: Object {
    @objc dynamic var calculationLine = ""
    @objc dynamic var resultLine = ""
}
