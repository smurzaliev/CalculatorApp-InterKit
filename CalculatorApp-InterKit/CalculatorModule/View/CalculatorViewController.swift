//
//  CalculatorView.swift
//  CalculatorApp-InterKit
//
//  Created by Samat Murzaliev on 23.05.2022.
//

import UIKit
import SnapKit

class CalculatorViewController: UIViewController {
    
    //MARK: - Protocol
    var presenter: CalculatorViewPresenterProtocol!
    
    //MARK: - UI Elements
    private let buttonSymbols = ["C", "()", "%", "÷", "7", "8", "9", "x", "4", "5", "6", "-", "1", "2", "3", "+", "+/-", "0", ".", "="]
    private let buttonBackColors = ["clearButtonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "buttonBackColor", "equalButtonbackColor",]
    private let buttonTextColors = ["clearButtonTextColor", "equalButtonbackColor", "equalButtonbackColor", "equalButtonbackColor", "numButtonTextColor", "numButtonTextColor", "numButtonTextColor", "equalButtonbackColor", "numButtonTextColor", "numButtonTextColor", "numButtonTextColor", "equalButtonbackColor", "numButtonTextColor", "numButtonTextColor", "numButtonTextColor", "equalButtonbackColor", "numButtonTextColor", "numButtonTextColor", "numButtonTextColor", "equalButtonTextColor"]
    
    private lazy var calculationLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 48, weight: .regular)
        view.adjustsFontSizeToFitWidth = true
        view.textAlignment = .right
        view.textColor = UIColor(named: "itemTintColor")
        return view
    }()
    
    private lazy var resultLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 48, weight: .regular)
        view.adjustsFontSizeToFitWidth = true
        view.textAlignment = .right
        view.textColor = UIColor(hex: "969696", alpha: 1)
        return view
    }()
    
    private lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "separatorLineColor")
        return view
    }()
    
    private lazy var historyButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "Time Machine"), for: .normal)
        view.tintColor = UIColor(named: "itemTintColor")
        view.addTarget(self, action: #selector(historyTapped(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var rulerButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "Ruler"), for: .normal)
        view.tintColor = UIColor(named: "itemTintColor")
        return view
    }()
    
    private lazy var rootButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "Root"), for: .normal)
        view.tintColor = UIColor(named: "itemTintColor")
        return view
    }()
    
    private lazy var backspaceButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "Backspace"), for: .normal)
        view.tintColor = UIColor(named: "itemTintColor")
        return view
    }()
    
    private lazy var buttonCollectionVertical: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 20
        view.distribution = .fillEqually
        return view
    }()
    
    private lazy var buttonRowVertical1: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = 20
        view.distribution = .fillEqually
        return view
    }()
    private lazy var buttonRowVertical2: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = 20
        view.distribution = .fillEqually
        return view
    }()
    private lazy var buttonRowVertical3: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = 20
        view.distribution = .fillEqually
        return view
    }()
    private lazy var buttonRowVertical4: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = 20
        view.distribution = .fillEqually
        return view
    }()
    private lazy var buttonRowVertical5: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = 20
        view.distribution = .fillEqually
        return view
    }()
    
    //MARK: - ViewDidLoad Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setSubviews()
        presenter = CalculatorPresenter(view: self)
    }
    
    //MARK: - Adding and Setting Views
    func setSubviews() {
        view.backgroundColor = UIColor(named: "backgoundColor")
        
        view.addSubview(buttonCollectionVertical)
        buttonCollectionVertical.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(19)
            make.right.equalToSuperview().offset(-19)
            make.top.equalTo(view.frame.height * 0.404)
            make.bottom.equalToSuperview().offset(-33)
        }
        
        buttonCollectionVertical.addArrangedSubview(buttonRowVertical1)
        for i in 0...3 {
            let addButton = makeButon(title: buttonSymbols[i], backColor: buttonBackColors[i], textColor: buttonTextColors[i])
            buttonRowVertical1.addArrangedSubview(addButton)
        }
        
        buttonCollectionVertical.addArrangedSubview(buttonRowVertical2)
        for i in 4...7 {
            let addButton = makeButon(title: buttonSymbols[i], backColor: buttonBackColors[i], textColor: buttonTextColors[i])
            buttonRowVertical2.addArrangedSubview(addButton)
        }
        buttonCollectionVertical.addArrangedSubview(buttonRowVertical3)
        for i in 8...11 {
            let addButton = makeButon(title: buttonSymbols[i], backColor: buttonBackColors[i], textColor: buttonTextColors[i])
            buttonRowVertical3.addArrangedSubview(addButton)
        }
        buttonCollectionVertical.addArrangedSubview(buttonRowVertical4)
        for i in 12...15 {
            let addButton = makeButon(title: buttonSymbols[i], backColor: buttonBackColors[i], textColor: buttonTextColors[i])
            buttonRowVertical4.addArrangedSubview(addButton)
        }
        buttonCollectionVertical.addArrangedSubview(buttonRowVertical5)
        for i in 16...19 {
            let addButton = makeButon(title: buttonSymbols[i], backColor: buttonBackColors[i], textColor: buttonTextColors[i])
            buttonRowVertical5.addArrangedSubview(addButton)
        }
        
        view.addSubview(separatorLine)
        separatorLine.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(19)
            make.right.equalToSuperview().offset(-19)
            make.height.equalTo(1)
            make.bottom.equalTo(buttonCollectionVertical.snp.top).offset(-25)
        }
        
        view.addSubview(historyButton)
        historyButton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(19)
            make.bottom.equalTo(separatorLine.snp.top).offset(-20)
        }
        
        view.addSubview(rulerButton)
        rulerButton.snp.makeConstraints { make in
            make.left.equalTo(historyButton.snp.right).offset(25)
            make.bottom.equalTo(separatorLine.snp.top).offset(-20)
        }
        
        view.addSubview(rootButton)
        rootButton.snp.makeConstraints { make in
            make.left.equalTo(rulerButton.snp.right).offset(25)
            make.bottom.equalTo(separatorLine.snp.top).offset(-20)
        }
        
        view.addSubview(backspaceButton)
        backspaceButton.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-19)
            make.bottom.equalTo(separatorLine.snp.top).offset(-20)
        }
        
        view.addSubview(calculationLabel)
        resultLabel.font = .systemFont(ofSize: view.frame.width / 8, weight: .regular)
        calculationLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(19)
            make.right.equalToSuperview().offset(-19)
            make.top.equalTo(view.safeArea.top).offset(10)
        }
        
        view.addSubview(resultLabel)
        resultLabel.font = .systemFont(ofSize: view.frame.width / 8, weight: .regular)
        resultLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(19)
            make.right.equalToSuperview().offset(-19)
            make.bottom.equalTo(backspaceButton.snp.top).offset(view.frame.width / 15 * -1)
        }
    }
    
    func makeButon(title: String, backColor: String, textColor: String) -> UIButton {
        let button =  UIButton(type: .system)
        button.setTitle(title, for: .normal)
        button.backgroundColor = UIColor(named: backColor)
        button.layer.cornerRadius = 9
        button.tintColor = UIColor(named: textColor)
        button.titleLabel?.font = .systemFont(ofSize: (view.frame.width / 10) - 3, weight: .regular)
        button.addTarget(self, action: #selector(buttonTapped(view:)), for: .touchUpInside)
        return button
    }
    
    //MARK: - ButtonTapped func
    
    @objc func buttonTapped(view: UIButton) {
        presenter.calculatorButtonPressed(button: view)
    }
    
    @objc func historyTapped(view: UIButton) {
        present(HistoryViewController(), animated: true)
    }
}

extension CalculatorViewController: CalculatorViewProtocol {
    
    func updateExpressions(_ expression: String, _ result: String) {
        self.calculationLabel.text = expression
        self.resultLabel.text = result
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
}
