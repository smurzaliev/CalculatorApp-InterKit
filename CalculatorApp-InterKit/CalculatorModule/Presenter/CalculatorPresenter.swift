//
//  CalculatorPresenter.swift
//  CalculatorApp-InterKit
//
//  Created by Samat Murzaliev on 24.05.2022.
//

import UIKit
import RealmSwift

protocol CalculatorViewProtocol: AnyObject {
    func updateExpressions(_ expression: String, _ result: String)
}

protocol CalculatorViewPresenterProtocol: AnyObject {
    init(view: CalculatorViewProtocol)
    func calculatorButtonPressed(button: UIButton)
}

class CalculatorPresenter: CalculatorViewPresenterProtocol {
    let view: CalculatorViewProtocol
    let dataBaseManager = RealmManager.sharedInstance
    private lazy var currentExpression: String = ""
    private lazy var resultExpression: String = ""
    private lazy var lastDot = false
    private lazy var lastOperation = false
    private lazy var equalPressed = false
    
    required init(view: CalculatorViewProtocol) {
        self.view = view
    }
    
    //MARK: - Calculation Logic
    func calculatorButtonPressed(button: UIButton) {
        if let currentOperation = button.currentTitle {
            
            switch currentOperation {
            case "C":
                currentExpression = ""
                resultExpression = ""
                equalPressed = false
            case "+":
                if lastDot {
                    currentExpression = currentExpression + "0" + currentOperation
                    lastDot = false
                } else if !lastOperation {
                    currentExpression += currentOperation
                }
                lastOperation = true
                equalPressed = false
            case "-":
                if lastDot {
                    currentExpression = currentExpression + "0" + currentOperation
                    lastDot = false
                } else if !lastOperation {
                    currentExpression += currentOperation
                }
                lastOperation = true
                equalPressed = false
            case "x":
                if lastDot {
                    currentExpression = currentExpression + "0*"
                    lastDot = false

                } else if !lastOperation {
                    currentExpression += "*"
                }
                lastOperation = true
                equalPressed = false
            case "÷":
                if lastDot {
                    currentExpression = currentExpression + "0/"
                    lastDot = false
                } else if !lastOperation {
                    currentExpression += "/"
                }
                lastOperation = true
                equalPressed = false
            case "+/-":
                currentExpression += ""
            case "=":
                if lastDot {
                    currentExpression = currentExpression + "0"
                    resultExpression = calculateOperation(expression: currentExpression)
                    lastDot = false
                }
                if !lastOperation {
                    resultExpression = calculateOperation(expression: currentExpression)
                    lastDot = false
                    lastOperation = false
                } else {
                    break
                }
                if !equalPressed {
                    let newLine = HistoryModel()
                    newLine.calculationLine = currentExpression
                    newLine.resultLine = resultExpression
                    dataBaseManager.save(object: newLine)
                }
                equalPressed = true
            case ".":
                if lastOperation {
                    currentExpression += "0"
                }
                if canWePutDot(expression: currentExpression) {
                    currentExpression += currentOperation
                    lastDot = true
                }
                equalPressed = false
            default:
                currentExpression += currentOperation
                lastDot = false
                lastOperation = false
                equalPressed = false
            }
        }
        view.updateExpressions(currentExpression, resultExpression)
    }
    
    //MARK: - Calculation via NSExpression
    private func calculateOperation(expression: String) -> String {
        if let num = expression.calculate() {
            return String(num)
        } else {
            return ""
        }
    }
    
    //MARK: - Method for checking dots
    private func canWePutDot(expression: String) -> Bool {
        var status = true
        var methodIndex = 0
        for char in "+-/*()" {
            if let i = expression.lastIndex(of: char) {
                let index: Int = expression.distance(from: expression.startIndex, to: i)
                if index >= methodIndex {
                    methodIndex = index
                }
            }
        }
        if let i = expression.lastIndex(of: ".") {
            let index: Int = expression.distance(from: expression.startIndex, to: i)
            if index >= methodIndex {
                status = false
            } else {
                status = true
            }
        }
        return status
    }
}
